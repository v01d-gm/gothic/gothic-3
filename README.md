# Gothic 3

Steam, build 1037023/1037180

## How To Run

### Prepare your environment (once)

1. Install [uv](https://docs.astral.sh/uv/)

    Using scoop

    ```bash
    scoop install uv
    ```

    Without scoop

    ```ps1
    powershell -ExecutionPolicy ByPass -c "irm https://astral.sh/uv/install.ps1 | iex"
    ```

2. Install [MS C++ Build Tools](https://visualstudio.microsoft.com/visual-cpp-build-tools/)
as they are required to build some dependencies

### Run

#### Using [Make](https://www.gnu.org/software/make/)

```bash
make
```

#### If Make is not installed

```bash
uv run python src/main.py
```
