from trainerbase.common.teleport import Teleport, Vector3

from objects import player_x, player_y, player_z


tp = Teleport(
    player_x,
    player_y,
    player_z,
    labels={
        "@Ishtar / Secret Room": (-2075.630615234375, -1551.82666015625, 111.00639343261719),
    },
    dash_coefficients=Vector3(7, 7, 7),
    minimal_movement_vector_length=2,
)
