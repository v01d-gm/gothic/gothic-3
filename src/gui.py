from typing import override

from dearpygui import dearpygui as dpg
from trainerbase.common.keyboard import SimpleHotkeyHandler
from trainerbase.common.tapper import Tapper, TappingDevice
from trainerbase.gui.helpers import add_components, simple_trainerbase_menu
from trainerbase.gui.injections import CodeInjectionUI
from trainerbase.gui.misc import HotkeyHandlerUI, SeparatorUI
from trainerbase.gui.objects import GameObjectUI
from trainerbase.gui.scripts import ScriptUI
from trainerbase.gui.speedhack import SpeedHackUI
from trainerbase.gui.tapper import TapperUI
from trainerbase.gui.teleport import TeleportUI
from trainerbase.process import kill_game_process

from injections import god_mode, super_jump
from objects import last_used_item_count, lp, xp
from scripts import XPMultiplier, multiply_xp
from teleport import tp


class XPMultiplierUI(ScriptUI[XPMultiplier]):
    @override
    def __init__(self, script: XPMultiplier, label: str, hotkey: str | None = None, *, tts_on_hotkey: bool = True):
        super().__init__(script, label, hotkey, tts_on_hotkey=tts_on_hotkey)
        self.factor_dpg_tag = f"{self.DPG_TAG_PREFIX}_factor"

    def add_to_ui(self) -> None:
        super().add_to_ui()
        dpg.add_slider_int(
            tag=self.factor_dpg_tag,
            label="XP Multiplier",
            min_value=1,
            max_value=10,
            clamped=True,
            default_value=5,
            callback=self.on_multiplier_factor_update,
        )

    def on_multiplier_factor_update(self):
        self.script.factor = dpg.get_value(self.factor_dpg_tag)


@simple_trainerbase_menu("Gothic 3", 750, 450)
def run_menu():
    with dpg.tab_bar():
        with dpg.tab(label="Main"):
            add_components(
                CodeInjectionUI(god_mode, "God Mode", "F1"),
                CodeInjectionUI(super_jump, "Super Jump", "F2"),
                SeparatorUI(),
                GameObjectUI(xp, "XP"),
                GameObjectUI(lp, "LP"),
                GameObjectUI(
                    last_used_item_count,
                    "Last Used Item Count",
                    set_hotkey="O",
                    default_setter_input_value=1000,
                ),
                SeparatorUI(),
                XPMultiplierUI(multiply_xp, "Multiply XP", "F3"),
                SeparatorUI(),
                HotkeyHandlerUI(SimpleHotkeyHandler(kill_game_process, "Ctrl+Shift+Alt+K"), "Kill Gothic 3 Process"),
            )

        with dpg.tab(label="Teleport"):
            add_components(TeleportUI(tp))

        with dpg.tab(label="Speedhack & Clickers"):
            add_components(
                SpeedHackUI(default_factor_input_value=7),
                SeparatorUI(),
                TapperUI(),
                SeparatorUI(),
                TapperUI(Tapper(0.25, "0", TappingDevice.KEYBOARD), "PageDown"),
            )
