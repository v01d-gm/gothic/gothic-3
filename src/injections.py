from trainerbase.codeinjection import AllocatingCodeInjection, CodeInjection, MultipleCodeInjection

from memory import base_of_game_dll, base_of_physxcore_dll, base_of_script_dll, last_used_item_count_pointer


god_mode = CodeInjection(
    base_of_script_dll + 0x1521E,
    """
        mov al, 1
        nop
    """,
)

super_jump = AllocatingCodeInjection(
    base_of_physxcore_dll + 0x1072E,
    """
        add eax, 666666
        mov [ecx + 0x24], eax
        mov edx, [ecx + 0x60]
    """,
    original_code_length=6,
)

update_last_used_item_count_pointer = MultipleCodeInjection(
    AllocatingCodeInjection(  # Get item
        base_of_game_dll + 0xC38B7,
        f"""
            mov [{last_used_item_count_pointer}], esi
            add [esi + 0x3C], ecx
            mov ecx, [edi + 0xC]
        """,
        original_code_length=6,
    ),
    AllocatingCodeInjection(  # Use item
        base_of_game_dll + 0xC3988,
        f"""
            mov [{last_used_item_count_pointer}], ebx
            sub [ebx + 0x3C], ebp
            cmp dword [edi + 0xC], 0
        """,
        original_code_length=7,
    ),
)
