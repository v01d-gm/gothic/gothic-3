from trainerbase.gameobject import GameFloat, GameUnsignedInt

from memory import last_used_item_count_address, player_coords_struct_address, player_exp_address


xp = GameUnsignedInt(player_exp_address + [0x54])
lp = GameUnsignedInt(player_exp_address + [0x58])

last_used_item_count = GameUnsignedInt(last_used_item_count_address)

player_x = GameFloat(player_coords_struct_address)
player_z = GameFloat(player_coords_struct_address + 0x4)
player_y = GameFloat(player_coords_struct_address + 0x8)
