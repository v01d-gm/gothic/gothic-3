from trainerbase.memory import Address, allocate, get_module_address


base_of_game_dll = get_module_address("Game.dll", delay=5, retry_limit=6)
base_of_physxcore_dll = get_module_address("PhysXCore.dll", delay=5, retry_limit=6)
base_of_script_dll = get_module_address("Script.dll", delay=5, retry_limit=10)

last_used_item_count_pointer = allocate()

player_exp_address = Address(base_of_script_dll + 0x66E00)
last_used_item_count_address = Address(last_used_item_count_pointer, [0x3C])
player_coords_struct_address = Address(base_of_script_dll + 0x66E04, [0xF4, 0x1C, 0x3C])
